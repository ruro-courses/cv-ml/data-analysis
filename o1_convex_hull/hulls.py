#!/usr/bin/python
from scipy.spatial import ConvexHull
from matplotlib import pyplot as plt
import numpy as np

x = np.asarray([0, 1, 3, 3])
y1 = np.asarray([0, 1, 2, 0])
y2 = (3**y1 - 1)/4

def plot_with_hull(x, y, **kw):
    print(np.hstack((x[:,np.newaxis],y[:,np.newaxis])))
    hull = ConvexHull(np.hstack((x[:,np.newaxis],y[:,np.newaxis])))
    plt.scatter(x, y, **kw)
    xh = x[hull.vertices]
    yh = y[hull.vertices]
    plt.plot(np.concatenate([xh, xh[:1]]), np.concatenate([yh, yh[:1]]), **kw)
    plt.legend()
    
plot_with_hull(x, y1, label='y1', color='C0')
plot_with_hull(x, y2, label='y2', color='C1')
plt.savefig('hulls.png')
