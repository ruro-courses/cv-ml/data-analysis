#!/bin/sh
mkdir -p janatahack-customer-segmentation
cd janatahack-customer-segmentation

kaggle datasets download -d vetrirah/customer
unzip -o customer.zip
rm -f customer.zip

kaggle datasets metadata vetrirah/customer
