import asyncio
import pyppeteer
import textwrap
import sys


async def main(password_text, url, outfile):
    print('Starting...')
    browser = await pyppeteer.launch()
    page = await browser.newPage()
    await page.emulateMedia('screen')
    await page.setViewport({'width': 1920, 'height': 1080})
    await page.goto(url)

    if password_text:
        print('Logging in...')
        password = await page.querySelector('#password_input')
        await password.focus()
        await page.waitFor(1000)
        await password.type(text=password_text)
        await page.waitFor(1000)

        login = await page.querySelector('#login_submit')
        await login.click()

    print('Loading page...')
    await page.waitFor(10000)

    print('Patching page contents...')
    await page.evaluate(textwrap.dedent(
        """\
        notebook = document.querySelector('#notebook-container')
        document.body.innerHTML = ''
        document.body.appendChild(notebook)
        notebook.style.width = '1200px'
        document.querySelector('.selected').classList.replace('selected', 'unselected')
        """
    ))

    await page.waitFor(1000)
    print("Fetching page height...")
    height = await page.evaluate('document.body.scrollHeight')
    
    print('Rendering pdf...')
    await page.waitFor(1000)
    await page.pdf({'path': outfile, 'width': 1200, 'height': 1 + height})


if __name__ == '__main__':
    asyncio.get_event_loop().run_until_complete(main(*sys.argv[1:]))
